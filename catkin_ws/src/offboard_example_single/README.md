# Single Agent Control Example

This package provides an example of how to control a single quadrotor running the PX4 firmware. It is designed to make the take-off and follow a square path. The package handles interface with the flight computer, putting it into offboard mode, reading its telemetry, and publishing quadrotor setpoints.

## Prerequisites

[PX4 Gazebo SITL packages](/GAZEBO_SITL_README.md)

## Instructions

open terminal and navigate to the px4 firmware folder and launch the SITL Gazebo simulation

```
make posix_sitl_default gazebo
```

In another terminal, navigate to `tx2-ros-examples/catkin_ws', source it, and launch MAVROS.

```
source devel/setup.bash
roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"
```

In another terminal, navigate to `tx2-ros-examples/catkin_ws', source it, and launch the single agent control ROS node.

```
source devel/setup.bash
rosrun offboard_single offboard_single_node
```

The quadrotor will take-off and move in a square pattern between four waypoints.