# Multi Agent Control Example

This package provides an example of how to control a swarm of quadrotors running the PX4 firmware. It is designed to make the take-off and avoid each other while creating a rotating, circular formation. The package handles interface with the flight computer, putting it into offboard mode, reading its telemetry, and publishing quadrotor setpoints. This package handles sharing raw GPS data to assist in collision avoidance. Quadrotor setpoints are created using simple gradient descent with indexed positions that move about a circular path as the targets and other quadrotor positions as the obstacles.     

## Prerequisites

[PX4 Gazebo SITL packages](/GAZEBO_SITL_README.md)

## Instructions

open terminal and navigate to the px4 firmware folder. 

If not done already, copy the configuration files ([iris_3](iris_3),[iris_4](iris_4),[iris_5](iris_5)) into `PX4-Autopilot/posix-configs/SITL/init/ekf2`.

```
cp ~/tx2-ros-examples/catkin_ws/src/offboard_example_multi/iris_* posix-configs/SITL/init/ekf2
```

Next execute the following commands

```
source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd):$(pwd)/Tools/sitl_gazebo
```

navigate to `tx2-ros-examples/catkin_ws/src/offboard_example_multi` and run

```
roslaunch offboard_multi_sitl.launch
```

This will launch the Gazebo simulator with 5 quadrotors. After some time, the terminal should display the following once for each quadrotor. 

```
[ INFO] [1610941498.248359645, 13.068000000]: HP: requesting home position
[ INFO] [1610941502.317775161, 17.130000000]: WP: mission received
```

In another terminal, navigate to `tx2-ros-examples/catkin_ws' and source it.

```
source devel/setup.bash
```

Then run the launch file that will create a node to control each of the quadrotors in the simulation

```
roslaunch offboard_multi multi_control.launch
```

The quadrotors will take-off and avoid each other while creating a rotating, circular formation.

### Running without the launch file

The multi_control.launch file is provided for conveient use with the simulator, but this package can be launched in terminal via

```
rosrun offboard_multi offboard_multi_node -i 0 -s 5
```

where the argument after -i is the assigned index and the argument after -s is the total number of agents in the swarm.


**_NOTE:_** If trying to run manually after launching offboard_multi_sitl.launch, you will need a separate terminal for each quadrotor. In each terminal you will need to the following before running the node. The number after uav should be 1 plus the argument passed in for -i.

```
export ROS_NAMESPACE=/uav1
```