# iBQR Code Repository

Welcome to the iBQR code repository. Here you will find useful examples for working with the iBQR.

## Prerequisites

[Ubuntu 16.04](https://releases.ubuntu.com/16.04/)\
[ROS Kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu)

## Package Specific Readme

[UWB Interface Node](catkin_ws/src/uwb_interface/README.md)\
[UWB Calibration](catkin_ws/src/uwb_delay_calibration/README.md)\
[Multi Agent Control Example](catkin_ws/src/offboard_example_multi/README.md)\
[Single Agent Control Example](catkin_ws/src/offboard_example_single/README.md)
<!-- [Multimaster ROS](catkin_ws/src/multimaster_example/README.md) -->

## Common Readme

[ROS Kinetic\MAVROS\Gazebo Setup](ROS_MAVROS_GAZEBO_README.md)\
[PX4 Gazebo SITL Simulator Setup](GAZEBO_SITL_README.md)

### Helpful links

[PX4 Autopilot User Guide (1.8.2)](https://docs.px4.io/v1.8.2/en/)\
[PX4 Firmware (1.8.2)](https://github.com/PX4/PX4-Autopilot/tree/v1.8.2)




