# ROS Kinetic, MAVROS, and Gazebo Setup

ROS (Robot Operating System) is a meta-operating system used in robotics. It provides the services you would expect from an operating system, including hardware abstraction, low-level device control, implementation of commonly-used functionality, message-passing between processes, and package management. It also provides tools and libraries for obtaining, building, writing, and running code across multiple computers.

MAVROS is the "official" supported bridge between ROS and the MAVLink protocol. The MARVROS ROS package enables MAVLink extendable communication between computers running ROS, MAVLink enabled autopilots, and MAVLink enabled GCS. MAVLink is a very lightweight messaging protocol for communicating with drones (and between onboard drone components).

Gazebo is a 3D robotics simulator that can be used with the PX4 firmware for both software-in-the-loop (SITL) and hardware-in-the-loop (HITL) simulations.

This short readme provides instructions for installing ROS Kinetic, MAVROS, and the Gazebo simulator.

## Instructions

Open a terminal and navigate to the root directory of this repository. From there, add permissions to run the shell script and run it. 

```
chmod +x ubuntu_sim_ros_gazebo.sh
./ubuntu_sim_ros_gazebo.sh
```

If no issues occur, ROS, MAVROS and Gazebo will be installed. You may need to close and reopen your terminal for it to work correctly.