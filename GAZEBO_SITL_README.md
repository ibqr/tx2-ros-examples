# PX4 Gazebo SITL Simulator Setup

The PX4 firmware has packages for both software-in-the-loop (SITL) and hardware-in-the-loop (HITL) simulations within the Gazebo simulator. This readme will help you set up and run the SITL packages for Gazebo simulator so it can be used with other packages included in this repository.

## Prerequisites

[ROS Kinetic\MAVROS\Gazebo Setup](ROS_MAVROS_GAZEBO_README.md)

### Helpful links

[PX4 Drone Autopilot Firmware](https://github.com/PX4/PX4-Autopilot)

## Instructions

Open a terminal and navigate to the following directory (create it if it does not already exist):

```
~\src
```

Clone the [PX4 Drone Autopilot Firmware](https://github.com/PX4/PX4-Autopilot), change to version 1.8.2, and build the SITL.

```
git clone https://github.com/PX4/PX4-Autopilot
cd PX4-Autopilot
git checkout tags/v1.8.2
make posix_sitl_default gazebo
```

After a short while, the Gazebo simulator will launch. After pressing ENTER in the same terminal you can command the quadcopter to takeoff,

```
commander takeoff
```

or land,

```
commander land
```

or do a number of other things

```
help
```

Use `ctrl-c` in terminal to exit.





